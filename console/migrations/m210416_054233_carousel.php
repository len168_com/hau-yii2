<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

use yii\db\Migration;

/**
 * Class m210416_054233_carousel
 */
class m210416_054233_carousel extends Migration
{
    const TB_CAROUSEL = '{{%carousel}}'; // 轮播图
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TB_CAROUSEL, [
            'id' => $this->primaryKey()->unsigned()->unique(),
            'title' => $this->string(64)->notNull()->defaultValue('')->comment('标题说明'),
            'description' => $this->string(128)->notNull()->defaultValue('')->comment('描述'),
            'bar_color' => $this->string(7)->notNull()->defaultValue('')->comment('顶栏背景色如白色：#FFFFFF'),
            'pic' => $this->string(255)->notNull()->defaultValue('')->comment('图片链接'),
            'url' => $this->string(255)->notNull()->defaultValue('')->comment('关联链接'),
            'status' => $this->boolean()->notNull()->defaultValue(0)->comment('状态：0下架，1上架'),
            'position' => $this->boolean()->notNull()->defaultValue(0)->comment('位置：0首页顶部'),
            'sort_id' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('排序，越大越前'),
            'show_level' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)->comment('此会员等级以上才显示'),
            'open_type' => $this->string(12)->notNull()->defaultValue(0)->comment('打开链接方式'),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions . ' COMMENT "轮播图" ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (YII_ENV_DEV) {
            $this->dropTable(self::TB_CAROUSEL);
            return true;
        }
        return false;
    }
}
