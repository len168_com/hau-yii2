<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

use yii\db\Migration;

/**
 * Class m210416_054308_sms
 */
class m210416_054308_sms extends Migration
{
    const TB_SMS_TPL = '{{%sms_tpl}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TB_SMS_TPL, [
            'id' => $this->primaryKey()->unsigned()->unique(),
            'tpl_type' => $this->string(10)->notNull()->defaultValue('')->comment('模板类型：aliyun(阿里云)'),
            'tpl_label' => $this->string(30)->notNull()->defaultValue('')->comment('模板标签名，如登录验证码：login'),
            'tpl_sign' => $this->string(30)->notNull()->defaultValue('')->comment('模板sign'),
            'tpl_code' => $this->string(30)->notNull()->defaultValue('')->comment('模板CODE'),
            'tpl_name' => $this->string(30)->notNull()->defaultValue('')->comment('模板中文名称'),
            'tpl_param' => $this->string(500)->notNull()->defaultValue('')->comment('模板中参数对照传参，json格式如模板有参数`${code}`：{"${code}": "captcha"}'),
            'tpl_content' => $this->string(250)->notNull()->defaultValue('')->comment('模板内容'),
            'tpl_remark' => $this->string(250)->notNull()->defaultValue('')->comment('模板说明备注'),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions . ' COMMENT "短信模板表" ');
        $this->createIndex('idx-type-label', self::TB_SMS_TPL, ['tpl_type', 'tpl_label'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (YII_ENV_DEV) {
            $this->dropTable(self::TB_SMS_TPL);
            return true;
        }
        return false;
    }
}
