<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

use yii\db\Migration;

/**
 * Class m210416_054316_user
 */
class m210416_054316_user extends Migration
{
    const TB_USER = '{{%user}}'; // 用户表
    const TB_USER_EXTEND = '{{%user_extend}}'; // 用户相关的扩展字段表
    const TB_USER_OAUTH = '{{%user_oauth}}'; // 用户第三方账号表

    public function safeUp()
    {
        $tableOptions = $userOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
            $userOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB AUTO_INCREMENT=19527';
        }

        $this->createTable(self::TB_USER, [
            'id' => $this->primaryKey()->unsigned()->unique(),
            'nickname' => $this->string(20)->notNull()->unique()->comment('用户昵称'),
            'group_id' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0)->comment('管理员组id'),
            'country_code' => $this->smallInteger()->unsigned()->notNull()->defaultValue(86)->comment('手机号国家代码'),
            'mobile' => $this->bigInteger(11)->unsigned()->notNull()->defaultValue(0)->comment('手机号'),
            'avatar' => $this->string(128)->notNull()->defaultValue('')->comment('头像链接地址'),
            'password_hash' => $this->string(72)->notNull()->comment('账号密码'),
            'password_salt' => $this->char(20)->notNull()->comment('密码盐'),
            'access_token' => $this->string(32)->unique()->notNull()->comment('API登录令牌'),
            'access_token_expire' => $this->bigInteger()->unsigned()->notNull()->defaultValue(0)->comment('API登录令牌有效时长'),
            'status' => $this->boolean()->notNull()->defaultValue(10)->comment('账号状态：0注销，9禁用，10激活'),
            'user_status' => $this->boolean()->notNull()->defaultValue(1)->comment('用户状态：0异常，1正常'),
            'parent_1' => $this->integer(10)->unsigned()->notNull()->defaultValue(0)->comment('父级用户ID'),
            'password_error' => $this->integer(10)->unsigned()->notNull()->defaultValue(0)->comment('密码错误次数计数'),
            'invite_code' => $this->string(42)->notNull()->defaultValue('')->comment('邀请注册码'),
            'login_type' => $this->boolean()->unsigned()->notNull()->defaultValue(0)->comment('登录方式: 1密码，2手机验证码 3微信'),
            'login_ip' => $this->string(64)->notNull()->defaultValue('')->comment('登录ip'),
            'login_at' => $this->dateTime()->notNull()->defaultValue('2020-01-01 00:00:00')->comment('登录时间'),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $userOptions);
        $this->createIndex('idx-phone-status', self::TB_USER, ['country_code', 'mobile', 'status'], true);
        $this->createIndex('idx-parent_1', self::TB_USER, 'parent_1');


        $this->createTable(self::TB_USER_EXTEND, [
            'id' => $this->primaryKey()->unsigned()->unique(),
            'uid' => $this->integer(10)->unsigned()->notNull()->unique()->comment('用户表主键'),
            'sex' => $this->boolean()->notNull()->defaultValue(-1)->comment('性别:1男0女-1保密'),
            'province' => $this->boolean()->notNull()->defaultValue(0)->comment('省代码'),
            'city' => $this->smallInteger()->notNull()->defaultValue(0)->comment('市代码'),
            'area' => $this->integer()->notNull()->defaultValue(0)->comment('县/区代码'),
            'addr' => $this->string(64)->notNull()->defaultValue('')->comment('区域字符串：省,市,区'),
            'introduce' => $this->string(64)->notNull()->defaultValue('')->comment('自我简介'),
            'birthday' => $this->date()->comment('生日'),
            'version' => $this->integer(10)->unsigned()->notNull()->defaultValue(0)->comment('DB版本号, 防并发'),

            'updated_at' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime()->notNull()
        ], $tableOptions . ' COMMENT "用户属性扩展表" ');
        $this->createIndex('idx-uid', self::TB_USER_EXTEND, ['uid'], true);

        $this->createTable(self::TB_USER_OAUTH, [
            'id' => $this->primaryKey()->unsigned()->unique(),
            'uid' => $this->integer(10)->unsigned()->notNull()->comment('用户表主键'),
            'account' => $this->string(32)->defaultValue('')->comment('第三方账号id'),
            'avatar' => $this->string(255)->defaultValue('')->comment('第三方账号头像'),
            'oauth_type' => $this->boolean()->unsigned()->notNull()->comment('第三方类型: 0支付宝，1微信，2微博, 3淘宝'),
            'nickname' => $this->string(32)->defaultValue('')->comment('第三方账号昵称'),
            'sex' => $this->boolean()->defaultValue(-1)->comment('第三方账号性别'),
            'age' => $this->boolean()->unsigned()->defaultValue(0)->comment('第三方账号年龄'),
            'openid' => $this->string(32)->defaultValue('')->comment('第三方账号openid'),
            'unionid' => $this->string(32)->defaultValue('')->comment('第三方账号unionid'),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions . ' COMMENT "用户绑定第三方账号表" ');
        $this->createIndex('idx-unionid-oauth_type', self::TB_USER_OAUTH, ['unionid', 'oauth_type'], true);
        $this->createIndex('idx-uid-oauth_type', self::TB_USER_OAUTH, ['uid', 'oauth_type']);
    }

    public function safeDown()
    {
        if (YII_ENV_DEV) {
            $this->dropTable(self::TB_USER);
            $this->dropTable(self::TB_USER_EXTEND);
            $this->dropTable(self::TB_USER_OAUTH);
            return true;
        }
        return false;
    }
}
