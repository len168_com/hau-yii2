<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

use yii\db\Migration;

/**
 * Class m210416_054206_config
 */
class m210416_054206_config extends Migration
{
    const TB_CONFIG = '{{%config}}'; // 配置表
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TB_CONFIG, [
            'id' => $this->primaryKey()->unsigned()->unique(),
            'name' => $this->string(10)->unique()->comment('配置唯一名称'),
            'config_json' => $this->text()->notNull()->comment('普通配置'),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions . ' COMMENT "配置表" ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (YII_ENV_DEV) {
            $this->dropTable(self::TB_CONFIG);
            return true;
        }
        return false;
    }
}
