<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

use yii\db\Migration;

/**
 * Class m210416_054252_attachment
 */
class m210416_054252_attachment extends Migration
{
    const TB_ATTACHMENT = '{{%attachment}}'; // 附件表
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TB_ATTACHMENT, [
            'id' => $this->primaryKey()->unsigned()->unique(),
            'uid' => $this->integer(10)->unsigned()->notNull()->defaultValue(0)->comment('上传用户id'),
            'file_md5' => $this->string(32)->notNull()->defaultValue('')->comment('文件md5值'),
            'file_name' => $this->string(128)->notNull()->defaultValue('')->comment('文件名称'),
            'file_type' => $this->boolean()->notNull()->defaultValue(0)->comment('类型：0图片, 1视频，2音频，250其它'),
            'file_size' => $this->integer(10)->notNull()->defaultValue(0)->comment('文件大小字节'),
            'file_path' => $this->string(255)->notNull()->defaultValue('')->comment('文件路径'),
            'file_suffix' => $this->string(10)->notNull()->defaultValue('')->comment('文件后缀名'),
            'save_handler' => $this->string(20)->notNull()->defaultValue('')->comment('文件保存的方式：aliyunOss阿里云等'),
            'is_del' => $this->boolean()->notNull()->defaultValue(0)->comment('是否删除到回收站：0否, 1是'),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions . ' COMMENT "附件表" ');
        $this->createIndex('idx-uid', self::TB_ATTACHMENT, 'uid');
        $this->createIndex('idx-file_md5', self::TB_ATTACHMENT, 'file_md5', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (YII_ENV_DEV) {
            $this->dropTable(self::TB_ATTACHMENT);
            return true;
        }
        return false;
    }
}
