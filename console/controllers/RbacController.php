<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/17 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace console\controllers;

use common\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use common\rbac\DeleteRoleRule;

class RbacController extends Controller
{
    /**
     * 初始化权限
     * @return int
     * @throws \Exception
     */
    public function actionInit()
    {
        $auth = Yii::$app->getAuthManager();

        // 添加规则
        $deleteRole = new DeleteRoleRule();
        $auth->add($deleteRole);

        // 创建角色
        $admin = $auth->createRole('administrator');
        $admin->description = '超级管理员';
        $auth->add($admin);

        echo "begin init rbac!\n";
        $permissions = [
            ['name' => 'carouselList', 'desc' => '轮播图列表数据'],
            ['name' => 'carouselCreate', 'desc' => '创建轮播图'],
            ['name' => 'carouselUpdate', 'desc' => '更新轮播图'],
            ['name' => 'carouselDelete', 'desc' => '删除轮播图'],
            ['name' => 'configUpdateRecord', 'desc' => '更新配置'],
            ['name' => 'configGetRecord', 'desc' => '获取配置'],
            ['name' => 'menuList', 'desc' => '菜单列表数据'],
            ['name' => 'menuCreate', 'desc' => '创建菜单'],
            ['name' => 'menuUpdate', 'desc' => '更新菜单'],
            ['name' => 'permissionList', 'desc' => '权限列表数据'],
            ['name' => 'permissionCreate', 'desc' => '创建权限'],
            ['name' => 'permissionUpdate', 'desc' => '更新权限'],
            ['name' => 'roleList', 'desc' => '角色列表数据'],
            ['name' => 'roleUserList', 'desc' => '角色用户列表数据'],
            ['name' => 'roleSearchUser', 'desc' => '搜索未授权用户'],
            ['name' => 'roleAssignUser', 'desc' => '授权角色给用户'],
            ['name' => 'roleRevokeUser', 'desc' => '撤消授权角色给用户'],
            ['name' => 'roleAssignPermission', 'desc' => '绑定权限给角色'],
            ['name' => 'roleAssignMenu', 'desc' => '绑定菜单给角色'],
            ['name' => 'roleCreate', 'desc' => '创建角色'],
            ['name' => 'roleUpdate', 'desc' => '更新角色'],
            ['name' => 'ruleList', 'desc' => '规则列表数据'],
            ['name' => 'smsList', 'desc' => '短信模板列表数据'],
            ['name' => 'smsCreate', 'desc' => '创建短信模板'],
            ['name' => 'smsUpdate', 'desc' => '更新短信模板'],
            ['name' => 'smsDelete', 'desc' => '删除短信模板'],
            ['name' => 'userList', 'desc' => '用户列表数据'],
            ['name' => 'userDetail', 'desc' => '用户详情数据'],
            ['name' => 'attachmentList', 'desc' => '附件列表数据'],
            ['name' => 'attachmentRemove', 'desc' => '移除附件到回收站'],
            ['name' => 'attachmentDelete', 'desc' => '彻底删除附件'],
            ['name' => 'roleDelete', 'desc' => '删除角色'],
        ];
        foreach ($permissions as $permission) {
            try {
                $perm = $auth->createPermission($permission['name']);
                $perm->description = $permission['desc'];
                if ($permission['name'] == 'roleDelete') {
                    // 删除角色权限 绑定规则删除规则
                    $perm->ruleName = $deleteRole->name;
                }
                $auth->add($perm);
                // 绑定权限给超级管理员
                $auth->addChild($admin, $perm);
                echo "add [{$perm->name} - {$perm->description}] success!\n";
            } catch (\Exception $e) {
                echo "add [{$permission['name']} - {$permission['desc']}] error!\n";
            }
        }

        echo "Finish!\n";
        return ExitCode::OK;
    }

    /**
     * 绑定角色
     * @return int
     * @throws \Exception
     */
    public function actionSetRole()
    {
        echo "Bind an role to user ...\n"; // comment 提示当前操作
        $uid = (int) $this->prompt('Administrator user id:'); // 接收用户id
        $roleName = \yii\helpers\BaseConsole::input("Administrator role name: "); // 接收角色名称
        $auth = Yii::$app->getAuthManager();
        $role = $auth->getRole($roleName);
        $user = User::findOne($uid);
        if ($role && $user) {
            $auth->assign($role, $uid);
            echo "Assign success\n";
        }
        if (empty($user)) {
            echo "User is not found\n";
        }
        return ExitCode::OK;
    }
}