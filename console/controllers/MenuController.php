<?php /** @noinspection ALL */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/17 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace console\controllers;

use common\models\Menu;
use common\models\MenuRole;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class MenuController extends Controller
{
    /**
     * 初始化菜单
     * @return int
     * @throws \Exception
     */
    public function actionInit()
    {
        echo "begin init menu!\n";
        $menus = [
            ['id' => 1, 'parent_id' => 0, 'children' => 0, 'name' => '仪表盘', 'route' => 'dashboardIndex'],
            ['id' => 2, 'parent_id' => 0, 'children' => 2, 'name' => '系统', 'route' => 'system'],
            ['id' => 3, 'parent_id' => 0, 'children' => 0, 'name' => '附件管理', 'route' => 'attachmentIndex'],
            ['id' => 4, 'parent_id' => 0, 'children' => 4, 'name' => '权限管理', 'route' => 'rbac'],
            ['id' => 5, 'parent_id' => 0, 'children' => 1, 'name' => '用户管理', 'route' => 'user'],
            ['id' => 6, 'parent_id' => 2, 'children' => 2, 'name' => '设置', 'route' => 'systemSetting'],
            ['id' => 7, 'parent_id' => 2, 'children' => 2, 'name' => '站点', 'route' => 'systemSite'],
            ['id' => 8, 'parent_id' => 6, 'children' => 0, 'name' => '站点设置', 'route' => 'systemSettingSite'],
            ['id' => 9, 'parent_id' => 6, 'children' => 0, 'name' => '用户设置', 'route' => 'systemSettingUser'],
            ['id' => 10, 'parent_id' => 7, 'children' => 0, 'name' => '轮播图', 'route' => 'systemSiteCarousel'],
            ['id' => 11, 'parent_id' => 7, 'children' => 0, 'name' => '验证码', 'route' => 'systemSiteSms'],
            ['id' => 12, 'parent_id' => 4, 'children' => 0, 'name' => '角色', 'route' => 'rbacRole'],
            ['id' => 13, 'parent_id' => 4, 'children' => 0, 'name' => '权限', 'route' => 'rbacPermission'],
            ['id' => 14, 'parent_id' => 4, 'children' => 0, 'name' => '规则', 'route' => 'rbacRule'],
            ['id' => 15, 'parent_id' => 4, 'children' => 0, 'name' => '菜单', 'route' => 'rbacMenu'],
            ['id' => 16, 'parent_id' => 5, 'children' => 0, 'name' => '用户列表', 'route' => 'userList'],
        ];
        foreach ($menus as &$menu) {
            $menu['created_at'] = $menu['updated_at'] = date('Y-m-d H:i:s');
        }
        try {
            Yii::$app->getDb()->createCommand()->batchInsert(Menu::tableName(), ['id', 'parent_id', 'children', 'name', 'route', 'created_at', 'updated_at'], $menus)->execute();
        } catch (\Exception $e) {
            echo "Error:" . $e->getMessage();
            echo "\n";
        }
        echo "Finish!\n";
        return ExitCode::OK;
    }

    /**
     * 绑定全部菜单给超级管理员
     * @return int
     * @throws \Exception
     */
    public function actionSetToAdmin()
    {
        echo "Bind an role to menu ...\n"; // comment 提示当前操作
        $menus = [
            ['menu_id' => 1, 'role' => 'administrator'],
            ['menu_id' => 2, 'role' => 'administrator'],
            ['menu_id' => 3, 'role' => 'administrator'],
            ['menu_id' => 4, 'role' => 'administrator'],
            ['menu_id' => 5, 'role' => 'administrator'],
            ['menu_id' => 6, 'role' => 'administrator'],
            ['menu_id' => 7, 'role' => 'administrator'],
            ['menu_id' => 8, 'role' => 'administrator'],
            ['menu_id' => 9, 'role' => 'administrator'],
            ['menu_id' => 10, 'role' => 'administrator'],
            ['menu_id' => 11, 'role' => 'administrator'],
            ['menu_id' => 12, 'role' => 'administrator'],
            ['menu_id' => 13, 'role' => 'administrator'],
            ['menu_id' => 14, 'role' => 'administrator'],
            ['menu_id' => 15, 'role' => 'administrator'],
            ['menu_id' => 16, 'role' => 'administrator'],
        ];

        foreach ($menus as &$menu) {
            $menu['created_at'] = date('Y-m-d H:i:s');
        }
        try {
            Yii::$app->getDb()->createCommand()->batchInsert(MenuRole::tableName(), ['menu_id', 'role', 'created_at'], $menus)->execute();
        } catch (\Exception $e) {
            echo "Error:" . $e->getMessage();
            echo "\n";
        }
        echo "Finish! is success assign to administrator.\n";
        return ExitCode::OK;
    }
}