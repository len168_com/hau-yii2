<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_extend}}".
 *
 * @property int $id
 * @property int $uid 用户表主键
 * @property int $sex 性别:1男0女-1保密
 * @property int $province 省代码
 * @property int $city 市代码
 * @property int $area 县/区代码
 * @property string $addr 区域字符串：省,市,区
 * @property string $introduce 自我简介
 * @property string|null $birthday 生日
 * @property int $version DB版本号, 防并发
 * @property string $updated_at
 * @property string $created_at
 */
class UserExtend extends BaseActiveRecord
{
    const SEX_MAN = 1; //性别 男
    const SEX_WOMEN = 0; //性别 女
    const SEX_SECURITY = -1; // 性别 保密

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_extend}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'updated_at', 'created_at'], 'required'],
            [['uid', 'sex', 'province', 'city', 'area', 'version'], 'integer'],
            [['birthday', 'updated_at', 'created_at'], 'safe'],
            [['addr', 'introduce'], 'string', 'max' => 64],
            [['uid'], 'unique'],
            ['sex', 'default', 'value' => static::SEX_SECURITY],
            ['sex', 'in', 'range' => [static::SEX_SECURITY, static::SEX_MAN, static::SEX_WOMEN]]
        ];
    }

    /**
     * 用户表
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, [User::tableName() . '.id' => 'uid']);
    }
}
