<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */
namespace common\models;

use Yii;

/**
 * 用户资料模型
 * Profile form
 */
class ProfileForm extends BaseModel
{
    public $nickname; // 昵称
    public $avatar; // 头像
    public $introduce; // 简介
    public $sex; // 性别
    public $birthday; // 生日
    public $province; // 省
    public $city; // 市
    public $area; // 区
    public $addr; // 地区

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nickname', 'avatar', 'introduce', 'birthday', 'addr', 'sex'], 'trim'],
            [['sex'], 'integer']
        ];
    }

    /**
     * 昵称验证
     */
    public function validateNickname()
    {
        if (empty($this->nickname)) {
            $this->addError('nickname', '昵称不能为空');
        } else {
            if (strpos($this->nickname, '*') !== false) {
                $this->addError('nickname', '昵称不能含有*号');
            } else {
                $user = User::find()->where(['and', ['nickname' => $this->nickname], ['not in', 'id', Yii::$app->getUser()->getId()]])->one();
                if ($user) {
                    $this->addError('nickname', '此账号已被占用');
                }
            }
        }
    }

    /**
     * 简介验证
     */
    public function validateIntroduce()
    {
        if (empty($this->introduce)) {
            $this->addError('introduce', '简介不能为空');
        }
    }

    /**
     * 性别验证
     */
    public function validateSex()
    {
        if (!in_array($this->sex, [-1, 0, 1])) {
            $this->addError('sex', '性别无效');
        }
    }

    /**
     * 生日验证
     */
    public function validateBirthday()
    {
        $day = strtotime($this->birthday);
        if ($day <= 0 || $day > strtotime(date('Y-m-d'))) {
            $this->addError('birthday', '生日无效');
        }
    }

    /**
     * 修改昵称
     * @return boolean
     */
    public function editNickname()
    {
        $this->validateNickname();
        if (!$this->hasErrors()) {
            $user = Yii::$app->getUser()->getIdentity();
            $user->nickname = trim($this->nickname);
            $user->updated_at = date('Y-m-d H:i:s');
            return $user->update(true, ['nickname', 'updated_at']);
        }
        return false;
    }

    /**
     * 修改简介
     * @return boolean
     */
    public function editIntroduce()
    {
        $this->validateIntroduce();
        if (!$this->hasErrors()) {
            $user = Yii::$app->getUser()->getIdentity()->userExtend;
            $user->introduce = trim($this->introduce);
            $user->updated_at = date('Y-m-d H:i:s');
            return $user->update(false, ['introduce', 'updated_at']);
        }
        return false;
    }

    /**
     * 修改头像
     * @return boolean
     */
    public function editAvatar()
    {
        if (!empty($this->avatar)) {
            $user = Yii::$app->getUser()->getIdentity();
            $user->avatar = $this->avatar;
            $user->updated_at = date('Y-m-d H:i:s');
            return $user->update(false, ['avatar', 'updated_at']);
        }
        return false;
    }

    /**
     * 修改性别
     * @return boolean
     */
    public function editSex()
    {
        $this->validateSex();
        if (!$this->hasErrors()) {
            $user = Yii::$app->getUser()->getIdentity()->userExtend;
            $user->sex = intval($this->sex);
            $user->updated_at = date('Y-m-d H:i:s');
            return $user->update(false, ['sex', 'updated_at']);
        }
        return false;
    }

    /**
     * 修改生日
     * @return boolean
     */
    public function editBirthday()
    {
        $this->validateBirthday();
        if (!$this->hasErrors()) {
            $user = Yii::$app->getUser()->getIdentity()->userExtend;
            $user->birthday = $this->birthday;
            $user->updated_at = date('Y-m-d H:i:s');
            return $user->update(false, ['birthday', 'updated_at']);
        }
        return false;
    }

    /**
     * 修改地区
     * @return boolean
     */
    public function editAddr()
    {
        $this->validate();
        if (!$this->hasErrors()) {
            $user = Yii::$app->getUser()->getIdentity()->userExtend;
            $user->province = (int) $this->province;
            $user->city = (int) $this->city;
            $user->area = (int) $this->area;
            $user->addr = trim($this->addr);
            $user->updated_at = date('Y-m-d H:i:s');
            return $user->update(false, ['province', 'city', 'area',  'addr', 'updated_at']);
        }
        return false;
    }
}
