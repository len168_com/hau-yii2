<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\models;
use yii\base\Model;

/**
 * 公共基础模型类
 * Class Carousel
 */
class BaseModel extends Model
{
    /**
     * 获取表单第一个校验错误信息
     * @return string
     */
    public function getFirstErrorMessage()
    {
        $msg = '';
        if (is_array($this->errors)) {
            foreach ((array) $this->errors as $key => $error) {
                return $msg = $error[0];
            }
        }
        return $msg;
    }
}