<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_oauth}}".
 *
 * @property int $id
 * @property int $uid 用户表主键
 * @property string $account 第三方账号id
 * @property string $avatar 第三方账号头像
 * @property int $oauth_type 第三方类型: 0支付宝，1微信，2微博, 3淘宝
 * @property string $nickname 第三方账号昵称
 * @property int $sex 第三方账号性别
 * @property int $age 第三方账号年龄
 * @property string $openid 第三方账号openid
 * @property string $unionid 第三方账号unionid
 * @property string $expire_at 受权有效期
 * @property string $created_at
 * @property string $updated_at
 */
class UserOauth extends \yii\db\ActiveRecord
{
    const OAUTH_TYPE_ALIPAY = 0;
    const OAUTH_TYPE_WEIXIN = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_oauth}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'nickname', 'account', 'unionid', 'oauth_type', 'created_at', 'updated_at'], 'required'],
            [['uid', 'oauth_type', 'sex', 'age'], 'integer'],
            [['expire_at', 'created_at', 'updated_at'], 'safe'],
            [['avatar'], 'string', 'max' => 255],
            [['nickname', 'account', 'openid', 'unionid'], 'string', 'max' => 32],
            [['unionid', 'oauth_type'], 'unique', 'targetAttribute' => ['unionid', 'oauth_type']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'uid']);
    }
}
