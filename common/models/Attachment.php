<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%attachment}}".
 *
 * @property string $id
 * @property string $uid 上传用户id
 * @property string $file_md5 文件md5值
 * @property string $file_name 文件名称
 * @property int $file_type 类型：0图片, 1视频，2音频，5头像 10其它
 * @property int $file_size 文件大小字节
 * @property string $file_path 文件路径
 * @property string $file_suffix 文件后缀名
 * @property string $save_handler 文件保存的方式：aliyunOss阿里云, baiduOss百度云等
 * @property int $is_del 是否删除到回收站：0否, 1是
 * @property string $created_at
 * @property string $updated_at
 */
class Attachment extends \yii\db\ActiveRecord
{
    const IS_DEL_YES = 1;
    const IS_DEL_NO = 0;
    const IMAGE = 0; // 图片
    const VIDEO = 1; // 视频
    const AUDIO = 2; // 音频
    const AVATAR = 5;// 头像
    const OTHER = 127;// 其它文件
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%attachment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'file_type', 'file_size', 'is_del'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_md5'], 'string', 'max' => 32],
            [['file_name'], 'string', 'max' => 128],
            [['file_path'], 'string', 'max' => 255],
            [['file_suffix'], 'string', 'max' => 10],
            [['save_handler'], 'string', 'max' => 20],
            [['file_md5'], 'unique'],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'uid']);
    }

    /**
     * 移除文件到回收站
     * @param $id
     * @return bool|false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function removeFile($id)
    {
        if ($model = static::findOne($id)) {
            $model->is_del = static::IS_DEL_YES;
            return $model->update(false, ['is_del']);
        }
        return false;
    }

    /**
     * 彻底删除文件
     * @param $id
     * @return bool|false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function deleteFile($id)
    {
        $model = static::findOne($id);
        if ($model && $model->is_del == static::IS_DEL_YES) {
            $res = Yii::$app->oss->delete($model->file_path);
            if ($res['code'] === 1) { // 删除成功
                return $model->delete();
            }
        }
        return false;
    }
}
