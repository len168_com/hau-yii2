<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2021/4/20 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */
namespace common\rbac;

/**
 * 删除角色规则 检查如果是超级管理员角色不能删除
 * Class DeleteRoleRule
 */
class DeleteRoleRule extends \yii\rbac\Rule
{
    public $name = 'deleteRoleRule';
    public $description = '删除角色规则';

    /**
     * @param string|integer $user 用户 ID.
     * @param Item $item 该规则相关的角色或者权限
     * @param array $params 传给 ManagerInterface::checkAccess() 的参数
     * @return boolean 代表该规则相关的角色或者权限是否被允许
     */
    public function execute($user, $item, $params)
    {
        if ($params['role']->type == 1 && $params['role']->name == 'administrator') {
            return false;
        } else if ($params['role']->type == 1) {
            if (in_array($params['role']->name, $params['myRoles'])) {
                // 自己的角色也不能删除
                return false;
            }
        }
        return true;
    }
}