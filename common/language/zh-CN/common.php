<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 19-1-12 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

return [
    'Account has been locked.' => '此账号已被锁定!',
    'Account does not exist.' => '此账号不存在!',

    'This account is not exists!' => '此账号不存在!',
    'This username has already been taken.' => '此用户名已被使用.',
    'This nickname has already been taken.' => '此昵称已被使用.',
    'This email has already been taken.' => '此邮箱已被使用.',

    'Password is not the same as the input.' => '两次输入的密码不一样.',

    'Login failed multiple times, please try again later!' => '多次登录失败，请稍后再试!',

    'Incorrect username or password.' => '用户名或密码错误.',
    'Incorrect captcha.' => '验证码不正确.',


    'Must agree to the membership agreement.' => '必需同意注册协议.',
    'Must be English numeral underline combination.' => '必需是字母数字或下划线组合.',
    'Must be English begin and English numeral underline combination.' => '字母开头数字或下划线组合.',
];
