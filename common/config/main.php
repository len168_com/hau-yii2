<?php
return [
    'timeZone' => 'Asia/Shanghai',
    'language' => 'zh-CN',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'charset' => 'utf-8',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            // uncomment if you want to cache RBAC items hierarchy
            'cache' => 'cache',
        ],
        'api' => 'common\components\Api',
        'util' => 'common\components\Util',
        'oauth' => 'common\components\oauth\Oauth',
        'config' => [
            'class' => 'common\components\Config'
        ],
        'alipayOauth' => [
            'class' => 'common\components\oauth\Alipay'
        ],
        'weixinOauth' => [
            'class' => 'common\components\oauth\Weixin'
        ],
        'payment' => [
            'class' => 'common\components\payment\Payment'
        ],
        'alipay' => [
            'class' => 'common\components\payment\Alipay'
        ],
        'wxpay' => [
            'class' => 'common\components\payment\Wxpay'
        ],
        'sms' => [
            'class' => 'common\components\sms\Sms'
        ],
        'aliyunSms' => [
            'class' => 'common\components\sms\AliyunSms'
        ],
        'oss' => [
            'class' => 'common\components\oss\Oss'
        ],
        'aliyunOss' => [
            'class' => 'common\components\oss\AliyunOss'
        ],
        'localOss' => [
            'class' => 'common\components\oss\LocalOss'
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/language',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'common' => 'common.php',
                    ],
                ],
            ],
        ],
    ],
];
