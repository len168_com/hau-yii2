<?php
return [
    'adminEmail' => 'admin@example.com', // 管理员邮箱
    'supportEmail' => 'support@example.com', // 技术支持邮箱
    'senderEmail' => 'noreply@example.com', // 发送邮箱
    'senderEmailName' => 'Example.com mailer', // 发送邮箱名称
];
