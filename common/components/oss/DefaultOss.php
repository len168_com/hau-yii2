<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/15 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\oss;

use Yii;
use yii\base\BaseObject;

abstract class DefaultOss extends BaseObject implements OssInterface
{
    /**
     * @param array $config 上传组件配置数组
     * @return mixed
     */
    abstract  public function setConfig($config);
    /**
     * 生成文件名称
     * @param $uid integer 上传用户ID
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function generateFileName($uid = 0)
    {
        return md5(Yii::$app->security->generateRandomString(5) . $uid);
    }

    /**
     * 生成文件路径
     * @param $uid integer 上传用户ID
     * @return mixed
     */
    public function generateFilePath($uid = 0)
    {
        return substr(md5($uid), 0, 6) . '/upload/' . date('Y-m-d') . '/' . $uid . '/';
    }

    /**
     * 生成头像文件路径
     * @param $uid integer 上传用户ID
     * @return mixed
     */
    public function generateAvatarFilePath($uid = 0)
    {
        return substr(md5($uid), 0, 6) . '/avatar/' . date('Y-m-d') . '/' . $uid . '/';
    }
}