<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/15 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */
namespace common\components\oss;

interface OssInterface
{
    /**
     * 上传文件
     * @param string $object OSS保存的文件对象名称
     * @param object &$file 要上传的本地文件对象 [@see \yii\web\UploadedFile::getInstanceByName()]
     * @param bool $deleteTempFile 是否删除临时文件.
     * @return mixed
     */
    public function upload($object, &$file, $deleteTempFile = true);

    /**
     * 删除文件
     * @param string $object 要删除的OSS对象文件路径
     * @return mixed
     */
    public function delete($object);

    /**
     * 创建文件名
     * @param $uid integer 上传用户ID
     * @return mixed
     */
    public function generateFileName($uid = 0);

    /**
     * 生成头像文件路径
     * @param $uid integer 上传用户ID
     * @return mixed|void
     */
    public function generateAvatarFilePath($uid = 0);
}
