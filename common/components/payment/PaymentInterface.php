<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/15 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */
namespace common\components\payment;
/**
 * 支付相关接口
 * Interface PaymentInterface
 * @package common\components\payment
 */
interface PaymentInterface
{
    /**
     * 生成支付订单
     * @param array $order 订单数组
     * @param string $form 生成的订单用处：app，h5等
     * @return mixed
     */
    public function createPayOrder($order = [], $form = 'app');

    /**
     * 异步通知验签
     * @param array $params 异步通知中收到的待验签的所有参数
     * @return mixed
     */
    public function verifyNotify($params = []);
}
