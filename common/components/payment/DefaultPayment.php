<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/15 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\payment;

use Yii;
use yii\base\BaseObject;

abstract class DefaultPayment extends BaseObject implements PaymentInterface
{
    /**
     * @param array $config 配置数组
     * @return mixed
     */
    abstract  public function setConfig($config);

    /**
     * 生成支付订单
     * @param array $order 订单数组
     * @param string $form 生成的订单用处：app，h5等
     * @return mixed
     */
    abstract public function createPayOrder($order = [], $form = 'app');


    /**
     * 转账接口
     * @param array $order
     * string $order['title'] 订单标题
     * string $order['no'] 商户订单号，64个字符以内，可包含字母、数字、下划线，需保证在商户端不重复
     * string $order['money'] 订单总金额，单位为元
     * string $order['account'] 唯一用户号
     * string $order['remark'] 转账备注
     * @return mixed
     */
    abstract public function transMoney($order = []);


    /**
     * 创建订单号
     * @param int $uid 用户id
     * @return string
     */
    abstract public function createTradeNo($uid);

    /**
     * 异步通知验签
     * @param array $params 异步通知中收到的待验签的所有参数
     */
    abstract public function verifyNotify($params = []);
}