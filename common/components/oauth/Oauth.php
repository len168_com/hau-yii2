<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/15 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\oauth;

use Yii;
use yii\base\BaseObject;

/**
 * Class Oauth 第三方登录
 * @package common\components\oauth
 */
class Oauth extends BaseObject
{
    const ALIPAY = 'alipayOauth'; // 支付宝
    const WEIXIN = 'weixinOauth'; // 微信
    /** @var object 处理类 */
    public $handler = null;
    public $oauthHandler = '';

    /**
     * 初始化
     */
    public function init()
    {
        if ($this->handler === null && !empty($this->oauthHandler)) {
            parent::init();
            $handler = $this->oauthHandler;
            $this->handler = Yii::$app->$handler;
            // 配置组件
            $this->handler->setConfig(Yii::$app->config->getItem($handler));
        }
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getToken($code)
    {
        $this->init();
        return $this->handler->getToken($code);
    }

    /**
     * @param $token
     * @return mixed
     */
    public function getUserShareInfo($token)
    {
        $this->init();
        return $this->handler->getUserShareInfo($token);
    }

    /**
     * @param string $token
     * @return mixed
     */
    public function refreshToken($token)
    {
        $this->init();
        return $this->handler->refreshToken($token);
    }

    /**
     * @return mixed
     */
    public function createOauthUrl()
    {
        $this->init();
        return $this->handler->createOauthUrl();
    }

    /**
     * @return mixed
     */
    public function createOauthInfo()
    {
        $this->init();
        return $this->handler->createOauthInfo();
    }
}