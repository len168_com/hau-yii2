<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/15 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\oauth;

use Yii;

/**
 * 微信
 * @package common\components\oauth
 */
class Weixin extends DefaultOauth
{
    private $_appId = '';
    private $_secret = '';

    /**
     * 初始化
     */
    public function init()
    {
        parent::init();
    }

    /**
     * 配置组件
     * @param array $config 配置数组
     */
    public function setConfig($config)
    {
        $this->_appId = isset($config['appId']) ? trim($config['appId']) : '';
        $this->_secret = isset($config['secret']) ? trim($config['secret']) : '';
    }

    /**
     * @param $code
     * @return array
     */
    public function getToken($code)
    {
        try {
            $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$this->_appId}&secret={$this->_secret}&code={$code}&grant_type=authorization_code";

            $data = Yii::$app->util->curlGet($url);
            if ($data && isset($data['access_token'])) {
                return ['status' => 1, 'data' => ['token' => $data['access_token']]];
            }
            return ['status' => 0, 'data' => [], 'message' => "接口调用失败"];
        } catch (\Exception $e) {
            return ['status' => 0, 'data' => [], 'message' => $e->getMessage()];
        }
    }

    /**
     * @param string $refreshToken
     * @return mixed|void
     */
    public function refreshToken($refreshToken)
    {
        // TODO: Implement refreshToken() method.
    }

    /**
     * @return mixed|void
     */
    public function createOauthUrl()
    {
        // TODO: Implement createOauthUrl() method.
    }

    /**
     * @param string $token
     * @return mixed|void
     */
    public function getUserShareInfo($token)
    {
        //todo
    }

    /**
     * 创建App端授权信息
     * @return mixed
     */
    public function createOauthInfo()
    {
        // TODO
    }
}
