<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/15 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\oauth;

use yii\base\BaseObject;

abstract class DefaultOauth extends BaseObject
{
    /**
     * @param array $config 配置数组
     * @return mixed
     */
    abstract  public function setConfig($config);

    /**
     * 生成H5授权页面url
     * @return mixed
     */
    abstract public function createOauthUrl();

    /**
     * 获取token
     * @param string $code 授权code
     * @return mixed
     */
    abstract public function getToken($code);

    /**
     * 获取用户资料
     * @param string $token 授权token
     * @return mixed
     */
    abstract public function getUserShareInfo($token);

    /**
     * 刷新token
     * @param string $refreshToken 刷新token
     * @return mixed
     */
    abstract public function refreshToken($refreshToken);

    /**
     * 生成App端授权信息
     * @return mixed
     */
    abstract public function createOauthInfo();
}
