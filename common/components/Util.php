<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/16 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components;

use yii\base\BaseObject;

/**
 * 公共助手函数类
 * Class Util
 * @package common\components
 */
class Util extends BaseObject
{
    /** @var string 缓存统一key前缀 */
    public static $cacheKeyPrefix = '__video_util_cache:';

    /**
     * 隐藏手机号
     * @param string $mobile 手机号
     * @return string
     */
    public static function hideMobile($mobile)
    {
        if (is_numeric($mobile) && mb_strlen($mobile) == 11 && static::regExp($mobile, 'cn_mobile')) {
            return substr($mobile, 0, 3) . '****' . substr($mobile, 7);
        }
        return $mobile;
    }

    /**
     * 单位分的金额转换为元
     * @param int $money
     * @return string
     */
    public static function moneyToYun($money = 0)
    {
        return floatval(sprintf('%.2f', $money / 100));
    }
    /**
     * 单位元的金额转换为分
     * @param int $money
     * @return integer
     */
    public static function yunToMoney($money = 0)
    {
        return (int) (sprintf('%.2f', $money * 100));
    }
    /**
     * 单位元的金额转换为元
     * @param int $money
     * @return integer
     */
    public static function yunToYun($money = 0)
    {
        return floatval(sprintf('%.2f', $money * 100 / 100));
    }

    /**
     * 单位元转换为积分
     * @param int $money 金额 元
     * @param int $points 积分=1元
     * @return integer
     */
    public static function yunToPoints($money = 0, $points = 0)
    {
        return (int) (sprintf('%.2f', $money * $points));
    }
    /**
     * 单位分转换为积分
     * @param int $money 金额 分
     * @param int $points 积分=1元
     * @return integer
     */
    public static function moneyToPoints($money = 0, $points = 0)
    {
        return (int) sprintf('%.2f', static::moneyToYun($money) * $points);
    }

    /**
     * 统一缓存键名
     * @param string $key 缓存key
     * @return string
     */
    public static function cacheKey($key = '')
    {
        return static::$cacheKeyPrefix . $key;
    }

    /**
     * 文本每行一条转换成数组
     * @param string $str
     * @return array|mixed
     * @throws \Exception
     */
    public static function textToArray($str = '')
    {
        if (empty($str)) return [];
        try {
            $str = str_replace(["\n", "\r\n"], "\r", $str);
            $str = explode("\r", $str);

            return $str;
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * 正则检查
     * @param {String} value 要检查的值
     * @param {Object} type 正则检查类型：cn_mobile手机号
     * @return bool|false|int
     */
    public static function regExp($value, $type)
    {
        if (empty($value)) {
            return false;
        }

        $res = false;
        switch ($type) {
            case 'cn_mobile':
                /**
                数据卡：14号段以前为上网卡专属号段，如中国联通的是145，中国移动的是147,中国电信的是149等等。
                虚拟运营商：170[1700/1701/1702]、162(电信)，1703/1705/1706、165(移动)，1704/1707/1708/1709(联通)、171、167（联通）
                卫星通信： 1740[0-5] (电信)，1349(移动)
                物联网网号：10648、1440 (移动)，10646、146(联通)，10649、1410(电信)
                国家工信部应急通信：1740[6-9]，1741[0-2]
                手机号码: 13[0-9], 14[5,6,7,8,9], 15[0-3, 5-9], 16[2,5,6,7], 17[0-8], 18[0-9], 19[0-3, 5-9]
                移动号段: 13[4-9],147,148,15[0-2,7-9],165,170[3,5,6],172,178,18[2-4,7-8],19[5,7,8]
                联通号段: 130,131,132,145,146,155,156,166,167,170[4,7,8,9],171,175,176,185,186,196
                电信号段: 133,149,153,162,170[0,1,2],173,174[0-5],177,180,181,189,19[0,1,3,9]
                广电号段: 192

                作者：bingxuePI
                链接：https://www.jianshu.com/p/5fbb85967bfd
                 */
                $res = preg_match('/^(13[0-9]|14[5-9]|15[0-3,5-9]|16[2,5,6,7]|17[0-8]|18[0-9]|19[0-3,5-9])\d{8}$/i', $value);
                break;
            case 'number':
                // 数字
                $res = preg_match('/^\d{1,}$/i', $value);
                break;
            case 'password':
                // 普通密码 只能包含字母、数字和下划线 长度在6~20之间
                $res = preg_match('/\w{6,20}/i', $value);
                break;
            case 'password_power':
                // 强密码 必须包含大小写字母和数字的组合，长度在8-20之间
                $res = preg_match('/^.*(?=.{8,20})(?=.*\d)(?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*[!@#$%^&*?\(\)]{0,}).*$/i', $value);
                break;
            case 'url':
                // 链接
                $res = preg_match('/http(s)?:\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(?::\d{1,5})?(?:$|[?\/#])/i', $value);
                break;
            default:
                break;
        }

        return $res;
    }

    /**
     * curl方式获取远程图片
     * @param string $url 图片链接
     * @param int $second 超时秒
     * @return bool|mixed
     */
    public function curlGetImage($url, $second = 30)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER,  false);
        curl_setopt($ch, CURLOPT_POST, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        if ($data) {
            return $data;
        }
        return '';
    }

    /**
     * curl方式请求
     * @param string $url 链接
     * @param int $second 超时秒
     * @return bool|mixed
     */
    public function curlGet($url, $second = 3)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER,  false);
        curl_setopt($ch, CURLOPT_POST, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        if ($data) {
            return $data;
        }
        return '';
    }
}
