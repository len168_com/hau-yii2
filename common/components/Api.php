<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 19-1-14 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components;

use yii\base\BaseObject;

/**
 * Api 统一数据返回格式
 * Class Api
 * @package api\components
 * @author toshcn <toshcn@foxmail.com>
 */
class Api extends BaseObject
{
    /**
     * @var int API status code
     */
    private $_statusCode = 1;

    /**
     * @var string the API status description that comes together with the status code.
     * @see $_apiStatuses
     */
    public $statusText = '';

    /**
     * @var array list of API status codes and the corresponding texts 状态码及其对应描述文本
     */
    private $_apiStatuses = [
        0 => 'error',
        1 => 'success',
        /* 权限错误: 1000-1999 */
        1001 => '登录失败',
        1002 => '注册失败',
        /* 资源错误: 3000-3999 */
        3001 => '用户不存在',
        /* 请求参数错误: 4000-4999 */
        4001 => '请求参数错误',
        4405 => '请求不合法，未按API要求发送POST请求',
        /* 服务器错误: 5000-5999 */
        5048 => '字段错误'
    ];

    /**
     * 获取当前Api状态码
     *
     * @return int API status codes 状态码
     */
    public function getStatusCode()
    {
        return $this->_statusCode;
    }

    /**
     * 获取Api状态码
     *
     * @return array API status codes 状态码
     */
    public function getApiStatus()
    {
        return $this->_apiStatuses;
    }

    /**
     * 设置Api返回码
     * @param int $value the API status code 状态码
     * @param string $text the status text. If not set, it will be set automatically based on the status code. 状态码描述文本
     * @return bool
     */
    public function setStatusCode($value, $text = null)
    {
        if (empty($value)) {
            $value = 0;
        }
        $this->_statusCode = (int) $value;
        if ($text === null) {
            $this->statusText = isset($this->_apiStatuses[$this->_statusCode]) ? $this->_apiStatuses[$this->_statusCode] : '';
        } else {
            $this->statusText = $text;
        }

        return true;
    }

    /**
     * 返回 格式化API
     *
     * @param int $statusCode the API status code 状态码
     * @param array $data the response content 要返回的数据内容
     * @param string $text the API status description that comes together with the status code. 状态码描述文本
     * @return array
     */
    public function send($statusCode, $data = [], $text = null)
    {
        $this->setStatusCode($statusCode, $text);
        return [
            'code' => (int) $statusCode,
            'message' => $this->statusText,
            'data' => $data,
        ];
    }

    /**
     * 返回成功 格式化API
     *
     * @param array $data the response content 要返回的数据内容
     * @param string $text the API status description that comes together with the status code. 状态码描述文本
     * @return array
     */
    public function success($data = [], $text = null)
    {
        $this->setStatusCode(1, $text);
        return [
            'code' => 1,
            'message' => $this->statusText,
            'data' => $data,
        ];
    }

    /**
     * 返回错误 格式化API
     *
     * @param string $text the API status description that comes together with the status code. 错误码描述文本
     * @return array
     */
    public function error($text = null)
    {
        $this->setStatusCode(0, $text);
        return [
            'code' => 0,
            'message' => $this->statusText,
            'data' => []
        ];
    }
}
