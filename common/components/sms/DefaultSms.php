<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\sms;

use Yii;
use yii\base\BaseObject;

abstract class DefaultSms extends BaseObject implements SmsInterface
{
    /**
     * 初始化缓存组件.
     */
    public function init()
    {
        parent::init();
    }

    /**
     * 单个发送短信
     * @param number $country [国家或地区码]
     * @param number $phone [手机号]
     * @param string $label 短信模板标签
     * @param array $param 短信模板变量数组
     * @return array|mixed
     */
    public function send($country, $phone, $label, $param = [])
    {
        $data = $this->formatData($country, $phone, $label, $param);

        return $this->sendData($data);
    }

    /**
     * 批量发送短信
     * @param array $country [国家或地区码]
     * @param array $phones 手机号，[手机号]
     * @param string $label 短信模板标签
     * @param array $param 短信模板变量数组
     * @return array|mixed
     */
    public function sendBatch($country, $phones, $label, $param = [])
    {
        $data = $this->formatBatchData($country, $phones, $label, $param);

        return $this->sendBatchData($data);
    }


    /**
     * 通过模板类型，模板签名查询模板
     * @param string $type 模板类型
     * @param string $label 模板标签名
     * @return false|null|array
     */
    abstract protected function querySmsTemplate($type, $label);

    /**
     * 生成单个发送短信接口需要的格式
     * @param number $country [国家或地区码]
     * @param number $phone [手机号]
     * @param string $label 短信模板标签
     * @param $param array 短信模板变量数组
     * @return array
     */
    abstract public function formatData($country, $phone, $label, $param = []);

    /**
     * 生成批量发送短信接口需要的格式
     * @param array $country [国家或地区码]
     * @param array $phones [手机号]
     * @param string $label 短信模板标签
     * @param $param array 短信模板变量数组
     * @return array
     */
    abstract public function formatBatchData($country, $phones, $label, $param = []);

    /**
     * 单个发送短信
     * @param $data array 短信接口数据
     * @return array ['code', 'message'] `code` 等于 0 发送失败, 1 发送成功. `message` 接口描述
     */
    abstract public function sendData($data);

    /**
     * 批量发送短信
     * @param $data array 短信接口数据
     * @return array ['code', 'message'] `code` 等于 0 发送失败, 1 发送成功. `message` 接口描述
     */
    abstract public function sendBatchData($data);
}