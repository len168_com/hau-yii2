<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\sms;

use Yii;
use yii\base\BaseObject;

class Sms extends BaseObject
{

    /** @var int 短信验证码长度 */
    public $captchaLength = 6;

    /** @var int 验证码缓存时间(秒) */
    public $captchaCacheDuration = 600;

    /** @var string 缓存前缀 */
    public $captchaCachePrefix = '__SMS_CAPTCHA/';

    /** @var null 短信处理对象 */
    public $handler = 'aliyunSms';

    /**
     * 初始化组件.
     */
    public function init()
    {
        parent::init();
        $handler = $this->handler;
        $this->captchaLength = Yii::$app->config->getSiteItem('smsCaptchaLength');
        $this->captchaCacheDuration = Yii::$app->config->getSiteItem('smsCaptchaCacheDuration');
        $this->handler = Yii::$app->$handler;
    }

    /**
     * 单个发送短信
     * @param number $country [国家或地区码]
     * @param number $phone [手机号]
     * @param string $label 短信模板标签
     * @param array $param 短信模板变量数组
     * @return mixed
     */
    public function send($country, $phone, $label, $param = [])
    {
        $data = $this->handler->formatData($country, $phone, $label, $param);

        return $this->handler->sendData($data);
    }

    /**
     * 批量发送短信
     * @param array $country [国家或地区码]
     * @param array $phones 手机号，[手机号]
     * @param string $label 短信模板标签
     * @param array $param 短信模板变量数组
     * @return array|mixed
     */
    public function sendBatch($country, $phones, $label, $param = [])
    {
        $data = $this->handler->formatBatchData($country, $phones, $label, $param);

        return $this->handler->sendBatchData($data);
    }

    /**
     * 验证短信验证码
     * @param string $captcha 验证码
     * @param number $phone [国家或地区码][手机号]
     * @param string $label 验证码用途
     * @return boolean
     */
    public function validateCaptcha($captcha, $phone, $label = '')
    {
        $key = Yii::$app->util->cacheKey($this->captchaCachePrefix . $phone . $label);
        if ($captcha && $captcha === Yii::$app->getCache()->get($key)) {
            Yii::$app->getCache()->delete($key);
            return true;
        }
        Yii::$app->getCache()->delete($key);
        return false;
    }

    /**
     * 生成短信验证码
     * @param number $phone [国家或地区码][手机号]
     * @param string $label 验证码用途
     * @return integer
     */
    public function generateCaptcha($phone, $label = '')
    {
        if ($this->captchaLength < 4) {
            $this->captchaLength = 4;
        }
        if ($this->captchaLength > 8) {
            $this->captchaLength = 8;
        }
        if ($this->captchaCacheDuration < 60) {
            $this->captchaCacheDuration = 60;
        }
        if ($this->captchaCacheDuration > 3600) {
            $this->captchaCacheDuration = 3600;
        }
        $letters = '0123456789';
        $code = '';
        for ($i = 0; $i < $this->captchaLength; ++$i) {
            $code .= $letters[mt_rand(0, 9)];
        }
        $key = Yii::$app->util->cacheKey($this->captchaCachePrefix . $phone . $label);
        Yii::$app->getCache()->set($key, $code, $this->captchaCacheDuration);

        return $code;
    }
}