<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/9/21 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace common\components\sms;

interface SmsInterface
{
    /**
     * 单个发送短信
     * @param number $country [国家或地区码]
     * @param number $phone [手机号]
     * @param string $label 短信模板标签
     * @param array $param 短信模板变量数组
     * @return array|mixed
     */
    public function send($country, $phone, $label, $param = []);

    /**
     * 批量发送短信
     * @param array $country [国家或地区码]
     * @param array $phones [手机号]
     * @param string $label 短信模板标签
     * @param array $param 短信模板变量数组
     * @return array|mixed
     */
    public function sendBatch($country, $phones, $label, $param = []);
}