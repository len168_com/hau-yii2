<?php /** @noinspection ALL */
/** @noinspection PhpUndefinedClassInspection */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\backend\controllers;

use common\models\Attachment;
use common\models\UserAccountBook;
use common\models\UserDepositBook;
use common\models\UserExtend;
use common\models\UserRewardBook;
use Yii;
use yii\data\Pagination;
use yii\db\StaleObjectException;
use api\controllers\AuthAdminController;
use common\models\PubinMoney;
use common\models\PubinMoneyForm;
use common\models\User;

/**
 * 后台附件接口 controller for the `backend` module
 */
class AttachmentController extends AuthAdminController
{
    /**
     * 获取列表
     * @return mixed
     */
    public function actionList()
    {
        $sort = trim(Yii::$app->getRequest()->get('sort'));
        $uid = intval(Yii::$app->getRequest()->get('uid'));
        $type = intval(Yii::$app->getRequest()->get('type'));
        $isdel = intval(Yii::$app->getRequest()->get('isdel', 0));
        $limit = intval(Yii::$app->getRequest()->get('limit', Yii::$app->config->getSiteItem('defaultPageSize')));
        $tbl = Attachment::tableName();
        switch ($sort) {
            case '-id':
                $orderBy["{$tbl}.id"] = SORT_DESC;
                break;
            case '+id':
                $orderBy["{$tbl}.id"] = SORT_ASC;
                break;
            default:
                $orderBy["{$tbl}.id"] = SORT_DESC;
        }
        $where = [];
        if ($uid) {
            $where["{$tbl}.uid"] = $uid;
        }
        if ($type > -1) {
            $where["file_type"] = $type;
        }
        $where["is_del"] = $isdel;

        $query = Attachment::find()
            ->orderBy($orderBy);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'defaultPageSize' => $limit]);

        $data = $query->select(["{$tbl}.*"])
            ->where($where)
            ->joinWith(['user' => function($q) {
                return $q->select(['id', 'nickname', 'avatar']);
            }])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();

        $page = (int) $pagination->getPageCount();
        $total = (int) $pagination->totalCount;
        unset($query, $pagination);
        return Yii::$app->api->success(['total' => $total, 'pageCount' => $page, 'items' => $data]);
    }

    /**
     * 移除文件到回收站
     */
    public function actionRemove()
    {
        $id = intval(Yii::$app->getRequest()->post('id'));
        if (Attachment::removeFile($id)) {
            return Yii::$app->api->success('移除成功');
        }
        return Yii::$app->api->error('移除失败');
    }

    /**
     * 彻底删除文件
     */
    public function actionDelete()
    {
        $id = intval(Yii::$app->getRequest()->post('id'));
        if (Attachment::deleteFile($id)) {
            return Yii::$app->api->success('彻底删除成功');
        }
        return Yii::$app->api->error('彻底删除失败');
    }
}
