<?php /** @noinspection ALL */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\backend\controllers;

use api\controllers\AuthAdminController;
use common\models\UserLevel;
use common\models\UserVip;
use Yii;

/**
 * Config controller for the `backend` module
 */
class ConfigController extends AuthAdminController
{
    /**
     * 更新配置
     * @return mixed
     */
    public function actionUpdateRecord()
    {
        $this->validateIsPost();
        try {
            if (Yii::$app->config->setConfigRecord(Yii::$app->getRequest()->post('data'), Yii::$app->getRequest()->post('name'))) {

                return Yii::$app->api->success();
            }
        } catch (\Exception $e) {
            return Yii::$app->api->error($e->getMessage());
        }
        return Yii::$app->api->error('更新失败');
    }

    /**
     * 配置页面数据
     * @return mixed
     */
    public function actionGetRecord()
    {
        $name = Yii::$app->getRequest()->get('name');
        if ($name == Yii::$app->config::CONFIG_TYPE_SITE) {
            return Yii::$app->api->success(Yii::$app->config->getDbConfig(Yii::$app->config::CONFIG_TYPE_SITE));
        } else if ($name == Yii::$app->config::CONFIG_TYPE_USER) {
            return Yii::$app->api->success(Yii::$app->config->getDbConfig(Yii::$app->config::CONFIG_TYPE_USER));
        }
        return Yii::$app->api->success();
    }
}
