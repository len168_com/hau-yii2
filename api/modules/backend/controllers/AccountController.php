<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 19-1-12 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\backend\controllers;

use Yii;
use api\controllers\BaseController;
use common\models\LoginForm;

/**
 * 账户 接口
 * Class AccountController
 * @package api\modules\v1\controllers
 * @author toshcn <toshcn@foxmail.com>
 */
class AccountController extends BaseController
{
    /**
     * 用户登录
     * @return mixed
     * @throws \Throwable
     */
    public function actionLogin()
    {
        $this->validateIsPost();
        $model = new LoginForm(['scenario' => Yii::$app->request->post('type') . '_login']);
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate() && $model->adminLogin()) {
            // 登录成功返回用户资料
            return Yii::$app->api->success($model->rebackAdminInfo());
        } else if ($model->hasErrors()) {
            return Yii::$app->api->error($model->getFirstErrorMessage());
        }
        return Yii::$app->api->error('登录失败');
    }
}
