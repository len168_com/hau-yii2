<?php /** @noinspection ALL */
/** @noinspection PhpUndefinedClassInspection */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\backend\controllers;

use common\models\Carousel;
use Yii;
use yii\db\StaleObjectException;
use api\controllers\AuthAdminController;

/**
 * 后台轮播图接口 controller for the `backend` module
 */
class CarouselController extends AuthAdminController
{
    /**
     * 获取列表
     * @return mixed
     */
    public function actionList()
    {
        $sort = trim(Yii::$app->getRequest()->get('sort_v'));
        switch ($sort) {
            case '-id':
                $orderBy['id'] = SORT_DESC;
                break;
            case '+id':
                $orderBy['id'] = SORT_ASC;
                break;
            case '-sort':
                $orderBy['sort_id'] = SORT_DESC;
                break;
            case '+sort':
                $orderBy['sort_id'] = SORT_ASC;
                break;
            default:
                $orderBy['id'] = SORT_DESC;
        }

        $data = Carousel::find()
            ->orderBy($orderBy)
            ->all();

        return Yii::$app->api->success($data);
    }

    /**
     * 创建
     */
    public function actionCreate()
    {
        $this->validateIsPost();
        $model = new Carousel();
        $model->load(Yii::$app->getRequest()->post(), '');
        if ($model->create()) {
            return Yii::$app->api->success(['id' => $model->id]);
        }
        if ($model->hasErrors()) {
            return Yii::$app->api->error($model->getFirstErrorMessage());
        }

        return Yii::$app->api->error('创建失败');
    }

    /**
     * 更新
     */
    public function actionUpdate()
    {
        $this->validateIsPost();
        $model = Carousel::findOne(Yii::$app->getRequest()->post('id'));
        if ($model) {
            $model->load(Yii::$app->getRequest()->post(), '');
            if ($model->updateRow()) {
                return Yii::$app->api->success($model->attributes);
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
            return Yii::$app->api->error('更新失败');
        }
        return Yii::$app->api->error('分类不存在');
    }

    /**
     * 删除
     */
    public function actionDelete()
    {
        $this->validateIsPost();
        $model = Carousel::findOne(Yii::$app->getRequest()->post('id'));
        if ($model) {
            try {
                if ($model->delete()) {
                    return Yii::$app->api->success();
                }
                return Yii::$app->api->error('删除失败', $model->getFirstErrorMessage());
            } catch (StaleObjectException $e) {
                return Yii::$app->api->error('删除失败', $e->getMessage());
            } catch (\Throwable $e) {
                return Yii::$app->api->error('删除失败', $e->getMessage());
            }
        }
        return Yii::$app->api->error('分类不存在');
    }
}
