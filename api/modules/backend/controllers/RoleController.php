<?php /** @noinspection ALL */
/** @noinspection PhpUndefinedClassInspection */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\backend\controllers;

use common\models\User;
use Yii;
use yii\data\Pagination;
use common\models\Menu;
use common\models\MenuRole;
use api\controllers\AuthAdminController;

/**
 * 后台角色接口 controller for the `backend` module
 */
class RoleController extends AuthAdminController
{
    /**
     * 获取列表
     * @return mixed
     */
    public function actionList()
    {
        $auth = Yii::$app->getAuthManager();
        $roles =  $auth->getRoles();
        $data = [];
        foreach ($roles as &$item) {
            $arr = $auth->getPermissionsByRole($item->name);
            $permissions = [];//已授权的权限
            foreach ($arr as $datem) {
                $permissions[] = $datem->name;
            }
            $menus = MenuRole::findAllByRole($item->name);
            foreach ($menus as &$menu) {
                $menu = (int) $menu->menu_id;
            }
            $data[] = ['name' => $item->name, 'permissions' => $permissions, 'menus' => $menus, 'description' => $item->description];
        }
        return Yii::$app->api->success(['items' => $data]);
    }

    /**
     * 获取授权角色用户列表
     * @return mixed
     */
    public function actionUserList()
    {
        $auth = Yii::$app->getAuthManager();
        $name = trim(Yii::$app->getRequest()->get('role'));
        $users = $auth->getUserIdsByRole($name);
        $data = User::find()
            ->select(['id', 'nickname', 'avatar', 'mobile', 'status', 'user_status', 'login_type', 'login_ip', 'login_at', 'created_at'])
            ->where(['id' => $users])
            ->asArray()
            ->all();
        return Yii::$app->api->success(['items' => $data]);
    }

    /**
     * 搜索未授权用户
     * @return mixed
     */
    public function actionSearchUser()
    {
        $auth = Yii::$app->getAuthManager();
        $skey = trim(Yii::$app->getRequest()->get('skey'));
        $name = trim(Yii::$app->getRequest()->get('role'));
        $users = $auth->getUserIdsByRole($name);
        $data = User::find()
            ->select(['id', 'nickname'])
            ->where(['not in', 'id', $users])
            ->andWhere(['like', 'nickname', $skey])
            ->asArray()
            ->all();
        return Yii::$app->api->success(['items' => $data]);
    }

    /**
     * 授权用户
     * @return mixed
     */
    public function actionAssignUser()
    {
        $auth = Yii::$app->getAuthManager();
        $uid = intval(Yii::$app->getRequest()->post('uid'));
        $name = trim(Yii::$app->getRequest()->post('role'));

        try {
            $role = $auth->getRole($name);
            if ($auth->assign($role, $uid)) {
                return Yii::$app->api->success();
            }
        } catch (\Exception $e) {
            return Yii::$app->api->error();
        }
        return Yii::$app->api->error();
    }
    /**
     * 删除授权用户
     * @return mixed
     */
    public function actionRevokeUser()
    {
        $auth = Yii::$app->getAuthManager();
        $uid = intval(Yii::$app->getRequest()->post('uid'));
        $name = trim(Yii::$app->getRequest()->post('role'));
        if ($name == '超级管理员') {
            return Yii::$app->api->error('超级管理员不能删除');
        }
        try {
            $role = $auth->getRole($name);
            if ($auth->revoke($role, $uid)) {
                return Yii::$app->api->success('删除角色成功');
            }
        } catch (\Exception $e) {
            return Yii::$app->api->error();
        }

        return Yii::$app->api->error('删除角色失败');
    }

    /**
     * 授权权限
     * @return mixed
     * @throws \Exception
     */
    public function actionAssignPermission()
    {
        $auth = Yii::$app->getAuthManager();
        $name = trim(Yii::$app->getRequest()->post('role'));
        $permissions = Yii::$app->getRequest()->post('permissions');

        try {
            $trans = Yii::$app->getDb()->beginTransaction();
            if ($role = $auth->getRole($name)) {
                $olds = $auth->getPermissionsByRole($name);
                $arr = [];
                foreach ($olds as $old) {
                    $arr[] = $old->name;
                }
                // 增加的权限
                if ($add = array_diff($permissions, $arr)) {
                    foreach ($add as $item) {
                        $permission = $auth->getPermission($item);
                        $auth->addChild($role, $permission);
                    }
                }
                // 减少的权限
                if ($dec = array_diff($arr, $permissions)) {
                    foreach ($dec as $item) {
                        $permission = $auth->getPermission($item);
                        $auth->removeChild($role, $permission);
                    }
                }
            }
            $trans->commit();
            return Yii::$app->api->success('授权成功');
        } catch (\Exception $e) {
            $trans->rollBack();
        }

        return Yii::$app->api->error('授权失败');
    }
    /**
     * 绑定菜单
     * @return mixed
     * @throws \Exception
     */
    public function actionAssignMenu()
    {
        $name = trim(Yii::$app->getRequest()->post('role'));
        $menus = Yii::$app->getRequest()->post('menus');

        try {
            $trans = Yii::$app->getDb()->beginTransaction();
            $olds = MenuRole::findAllByRole($name);
            $arr = [];
            foreach ($olds as $old) {
                $arr[] = $old->menu_id;
            }
            // 增加的菜单
            if ($add = array_diff($menus, $arr)) {
                $data = [];
                $new = date('Y-m-d H:i:s');
                foreach ($add as $item) {
                    $data[] = ['role' => $name, 'menu_id' => (int)$item, 'created_at' => $new];
                }
                Yii::$app->getDb()->createCommand()
                    ->batchInsert(
                        MenuRole::tableName(),
                        ['role', 'menu_id', 'created_at'],
                        $data
                    )
                    ->execute();
            }
            // 减少的菜单
            if ($dec = array_diff($arr, $menus)) {
                MenuRole::deleteAll(['role' => $name, 'menu_id' => $dec]);
            }
            $trans->commit();
            return Yii::$app->api->success('操作成功');
        } catch (\Exception $e) {
            $trans->rollBack();
            return Yii::$app->api->error($e->getMessage());
        }

        return Yii::$app->api->error('操作失败');
    }

    /**
     * 创建角色
     * @return mixed
     * @throws \Exception
     */
    public function actionCreate()
    {
        $auth = Yii::$app->getAuthManager();
        $name = trim(Yii::$app->getRequest()->post('name'));
        $desc = trim(Yii::$app->getRequest()->post('description'));
        $role = $auth->createRole($name);
        $role->description = $desc;
        if ($name && $auth->add($role)) {
            return Yii::$app->api->success();
        }
        return Yii::$app->api->error();
    }

    /**
     * 更新角色
     * @return mixed
     * @throws \Exception
     */
    public function actionUpdate()
    {
        $auth = Yii::$app->getAuthManager();
        $name = trim(Yii::$app->getRequest()->post('name'));
        $newName = trim(Yii::$app->getRequest()->post('new_name'));
        $desc = trim(Yii::$app->getRequest()->post('description'));
        try {
            $role = $auth->getRole($name);
            $role->description = $desc;
            $role->name = $newName;
            $auth->update($name, $role);
            return Yii::$app->api->success('更新成功');
        } catch (\Exception $e) {
            return Yii::$app->api->error($e->getMessage());
        }
        return Yii::$app->api->error('更新失败');
    }

    /**
     * 删除角色
     * @return mixed
     * @throws \Exception
     */
    public function actionDelete()
    {
        $roleName = trim(Yii::$app->getRequest()->post('role'));
        $auth = Yii::$app->getAuthManager();
        $role = $auth->getRole($roleName);
        $myRoles = $auth->getRolesByUser(Yii::$app->getUser()->getId());
        $roles = [];
        foreach ($myRoles as $myRole) {
            $roles[] = $myRole->name;
        }
        if (Yii::$app->getUser()->can('roleDelete', ['role' => $role, 'myRoles' => $roles])) {
            $trans = Yii::$app->getDb()->beginTransaction();
            try {
                if ($auth->remove($role) && MenuRole::removeRole($roleName)) {
                    $trans->commit();
                    return Yii::$app->api->success('删除成功');
                }
            } catch (\Exception $e) {
                $trans->rollBack();
                return Yii::$app->api->error($e->getMessage());
            }
            return Yii::$app->api->error('删除失败');
        }
        return Yii::$app->api->error('您无此权限');
    }
}
