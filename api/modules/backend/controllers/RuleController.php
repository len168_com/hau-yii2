<?php /** @noinspection ALL */
/** @noinspection PhpUndefinedClassInspection */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\backend\controllers;

use Yii;
use api\controllers\AuthAdminController;

/**
 * 后台规则接口 controller for the `backend` module
 */
class RuleController extends AuthAdminController
{
    /**
     * 获取列表
     * @return mixed
     */
    public function actionList()
    {
        $auth = Yii::$app->getAuthManager();
        $rules =  $auth->getRules();
        $data = [];
        foreach ($rules as &$item) {
            $data[] = ['name' => $item->name, 'description' => isset($item->description) ? $item->description : ''];
        }
        return Yii::$app->api->success(['items' => $data]);
    }
}
