<?php /** @noinspection PhpUndefinedClassInspection */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\backend\controllers;

use Yii;
use yii\db\StaleObjectException;
use api\controllers\AuthAdminController;
use common\models\SmsTpl;

/**
 * 后台短信模板接口 controller for the `backend` module
 */
class SmsController extends AuthAdminController
{
    /**
     * 获取列表
     * @return mixed
     */
    public function actionList()
    {
        $sort = trim(Yii::$app->getRequest()->get('sort'));
        switch ($sort) {
            case '-id':
                $orderBy['id'] = SORT_DESC;
                break;
            case '+id':
                $orderBy['id'] = SORT_ASC;
                break;
            case '-sort':
                $orderBy['cate_sort'] = SORT_DESC;
                break;
            case '+sort':
                $orderBy['cate_sort'] = SORT_ASC;
                break;
            default:
                $orderBy['id'] = SORT_DESC;
        }

        $data = SmsTpl::find()
            ->orderBy($orderBy)
            ->all();

        return Yii::$app->api->success($data);
    }

    /**
     * 创建
     */
    public function actionCreate()
    {
        $this->validateIsPost();
        $model = new SmsTpl();
        $model->load(Yii::$app->getRequest()->post(), '');
        if ($model->create()) {
            return Yii::$app->api->success(['id' => $model->id]);
        }
        if ($model->hasErrors()) {
            return Yii::$app->api->error($model->getFirstErrorMessage());
        }

        return Yii::$app->api->error('创建失败');
    }

    /**
     * 更新
     */
    public function actionUpdate()
    {
        $this->validateIsPost();
        $model = SmsTpl::findOne(Yii::$app->getRequest()->post('id'));
        if ($model) {
            $model->load(Yii::$app->getRequest()->post(), '');
            if ($model->updateRow()) {
                return Yii::$app->api->success($model->attributes);
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
            return Yii::$app->api->error('更新失败');
        }
        return Yii::$app->api->error('分类不存在');
    }

    /**
     * 删除
     */
    public function actionDelete()
    {
        $this->validateIsPost();
        $model = SmsTpl::findOne(Yii::$app->getRequest()->post('id'));
        if ($model) {
            try {
                if ($model->delete()) {
                    return Yii::$app->api->success();
                }
                return Yii::$app->api->error($model->getFirstErrorMessage());
            } catch (StaleObjectException $e) {
                return Yii::$app->api->error($e->getMessage());
            } catch (\Throwable $e) {
                return Yii::$app->api->error($e->getMessage());
            }
        }
        return Yii::$app->api->error('模板不存在');
    }
}
