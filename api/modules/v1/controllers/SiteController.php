<?php /** @noinspection ALL */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\v1\controllers;

use common\models\InvitePoster;
use common\models\UserLevel;
use Yii;
use yii\helpers\FileHelper;
use api\controllers\BaseController;
use common\models\Carousel;
use common\models\User;
use common\models\Notice;
use common\models\Task;
use common\models\TaskCate;
use common\models\UserStore;
use common\models\UserVip;

/**
 * 不用登录 请求接口
 * Site controller for the `v1` module
 */
class SiteController extends BaseController
{
    /**
     * 获取站点配置信息
     * @return mixed
     */
    public function actionConfig()
    {
        $imageExt = $videoExt = [];

        $uploadImageExt = Yii::$app->config->getSiteItem('uploadImageExt');
        $uploadVideoExt = Yii::$app->config->getSiteItem('uploadVideoExt');

        foreach ((array) $uploadImageExt as $item) {
            if ($mime = FileHelper::getMimeTypeByExtension('.' . $item)) {
                $imageExt[] = $mime;
            }
        }
        foreach ((array) $uploadVideoExt as $item) {
            if ($mime = FileHelper::getMimeTypeByExtension('.' . $item)) {
                $videoExt[] = $mime;
            }
        }
        $userConfig = Yii::$app->config->getUserItems();
        $alipay = Yii::$app->config->getSiteItem('alipay');//注意不要泄漏密钥
        $wxpay = Yii::$app->config->getSiteItem('wxpay');//注意不要泄漏密钥
        $weixinOauth = Yii::$app->config->getSiteItem('weixinOauth');//注意不要泄漏密钥
        $alipayOauth = Yii::$app->config->getSiteItem('alipayOauth');//注意不要泄漏密钥
        return Yii::$app->api->success([
            'imageExtStr' => join(', ', (array) $uploadImageExt),
            'imageExt' => $imageExt,
            'imageMaxSize' => (int) Yii::$app->config->getSiteItem('uploadImageMaxSize'),
            'videoExtStr' => join(', ', (array) $uploadVideoExt),
            'videoExt' => $videoExt,
            'videoMaxSize' => (int) Yii::$app->config->getSiteItem('uploadVideoMaxSize'),
            'imgCaptchaLength' => Yii::$app->config->getSiteItem('imgCaptchaLength'),
            'smsCaptchaLength' => Yii::$app->config->getSiteItem('smsCaptchaLength'),
            'userRegisterOnValidateCaptcha' => (bool) Yii::$app->config->getItem('userRegisterOnValidateCaptcha'),
            'userConfig' => $userConfig,
            'inviteHost' => Yii::$app->config->getSiteItem('inviteHost'),
            'alipayIsUse' => $alipay['use'],//注意不要泄漏密钥
            'wxpayIsUse' => $wxpay['use'],//注意不要泄漏密钥
            'wxOauthIsUse' => $weixinOauth['use'],//注意不要泄漏密钥
            'alipayOauthIsUse' => $alipayOauth['use'],//注意不要泄漏密钥
            'appDownLink' => Yii::$app->config->getSiteItem('appDownLink'),
        ]);
    }
}
