<?php /** @noinspection ALL */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/5/25 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\v1\controllers;

use Yii;
use api\controllers\UpdateController as Controller;

/**
 * 无需登录 更新 接口
 * @package api\controllers
 */
class UpdateController extends Controller
{
}
