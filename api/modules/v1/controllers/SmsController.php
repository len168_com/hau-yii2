<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/5/25 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\v1\controllers;

use api\controllers\SmsController as Controller;

/**
 * 短信 接口
 * @package api\controllers
 */
class SmsController extends Controller
{
}
