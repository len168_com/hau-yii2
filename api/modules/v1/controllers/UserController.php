<?php /** @noinspection ALL */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\v1\controllers;

use common\models\InvitePoster;
use common\models\ProfileForm;
use common\models\SmsCaptchaForm;
use common\models\Store;
use common\models\Task;
use common\models\UserExtend;
use common\models\UserFollower;
use common\models\UserForbid;
use Yii;

/**
 * User controller for the `app h5` module
 */
class UserController extends \api\controllers\UserController
{
    /**
     * 获取短信验证码
     */
    public function actionSmsCaptcha()
    {
        $this->validateIsPost();
        $captcha = Yii::$app->getRequest()->post('captcha');
        // 验证图形验证码
        if (!Yii::$app->controller->createAction('captcha')->validate($captcha, (bool) Yii::$app->config->getSiteItem('captchaCaseSensitive'))) {
            return Yii::$app->api->error('图形验证码不正确');
        }
        return Yii::$app->api->success();//todo
        $model = new SmsCaptchaForm();
        $user = Yii::$app->getUser()->getIdentity('country');
        $model->countryCode = $user->country_code;
        $model->mobile = $user->mobile;
        $model->label = Yii::$app->getRequest()->post('label');
        if ($model->validate() && $model->sendCaptcha()) {

            return Yii::$app->api->success();
        }
        if ($model->hasErrors()) {
            return Yii::$app->api->error($model->getFirstErrorMessage());
        }

        return Yii::$app->api->error('验证码发送失败');
    }

    /**
     * 重置密码
     * @return mixed
     * @throws \Throwable
     */
    public function actionResetPassword()
    {
        $this->validateIsPost();
        $captcha = trim(Yii::$app->getRequest()->post('captcha'));
        $password = trim(Yii::$app->getRequest()->post('password'));
        $user = Yii::$app->getUser()->getIdentity();

        if ($user->resetPassword($captcha, $password)) {
            return Yii::$app->api->success('密码修改成功');
        }
        if ($user->hasErrors()) {
            return Yii::$app->api->error($user->getFirstErrorMessage());
        }
        return Yii::$app->api->error('密码修改成功');
    }

    /**
     * 修改账号
     */
    public function actionEditUsername()
    {return Yii::$app->api->error('登录账号不允许修改');//todo
        $this->validateIsPost();
        $model = new ProfileForm();
        if ($model->load(Yii::$app->getRequest()->post(), '')) {
            if (Yii::$app->util->regExp('cn_mobile', $model->username)) {
                return Yii::$app->api->error('账号不能是手机号');
            }
            if ($model->editUsername()) {
                return Yii::$app->api->success();
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
        }
        return Yii::$app->api->error('修改失败');
    }

    /**
     * 修改昵称
     */
    public function actionEditNickname()
    {
        $this->validateIsPost();
        $model = new ProfileForm();
        if ($model->load(Yii::$app->getRequest()->post(), '')) {
            if (Yii::$app->util->regExp('cn_mobile', $model->nickname)) {
                return Yii::$app->api->error('昵称不能是手机号');
            }
            if ($model->editNickname()) {
                return Yii::$app->api->success();
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
        }
        return Yii::$app->api->error('修改失败');
    }

    /**
     * 修改简介
     */
    public function actionEditIntroduce()
    {
        $this->validateIsPost();
        $model = new ProfileForm();
        if ($model->load(Yii::$app->getRequest()->post(), '')) {
            if ($model->editIntroduce()) {
                return Yii::$app->api->success();
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
        }
        return Yii::$app->api->error('修改失败');
    }

    /**
     * 修改性别
     */
    public function actionEditSex()
    {
        $this->validateIsPost();
        $model = new ProfileForm();
        if ($model->load(Yii::$app->getRequest()->post(), '')) {
            if ($model->editSex()) {
                return Yii::$app->api->success();
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
        }
        return Yii::$app->api->error('修改失败');
    }

    /**
     * 修改生日
     */
    public function actionEditBirthday()
    {
        $this->validateIsPost();
        $model = new ProfileForm();
        if ($model->load(Yii::$app->getRequest()->post(), '')) {
            if ($model->editBirthday()) {
                return Yii::$app->api->success();
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
        }
        return Yii::$app->api->error('修改失败');
    }

    /**
     * 修改地区
     */
    public function actionEditAddr()
    {
        $this->validateIsPost();
        $model = new ProfileForm();
        if ($model->load(Yii::$app->getRequest()->post(), '')) {
            if ($model->editAddr()) {
                return Yii::$app->api->success();
            }
            if ($model->hasErrors()) {
                return Yii::$app->api->error($model->getFirstErrorMessage());
            }
        }
        return Yii::$app->api->error('修改失败');
    }
}
