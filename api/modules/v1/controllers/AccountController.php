<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\v1\controllers;

use api\controllers\AccountController as controller;
use Yii;

/**
 * Site controller for the `pc` module
 */
class AccountController extends controller
{

}
