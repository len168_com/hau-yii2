<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\modules\v1\controllers;

use common\models\Carousel;
use Yii;
use api\controllers\AuthController;

/**
 * 轮播图接口
 * controller for the `v1` module
 */
class CarouselController extends AuthController
{
    /**
     * 首页顶部轮播图
     */
    public function actionList()
    {
        $data = Carousel::getListData(Carousel::POSITION_INDEX_PAGE_TOP);
        return Yii::$app->api->success($data);
    }
}
