<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-api',
            // 配置api接收JSON数据
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            // 配置api 返回JSON格式
            'format' => \yii\web\Response::FORMAT_JSON
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => null,
            'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
            'on afterLogin' => ['common\models\User', 'afterLoginHandler']
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => null,
        ],
        'api' => 'api\components\ApiResponse',
        'urlManager' => [
            'enablePrettyUrl' => true,
            //此属性决定是否开启严格的请求解析 如果设置为启用，请求的URL必须至少匹配 rules 中的一条规则
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                // 配置url
                //['class' => 'yii\rest\UrlRule', 'controller' => ['users' => 'api/backend/user']],
                'login' => 'account/login',
                'captcha' => 'account/captcha',
                'sms/captcha' => 'sms/captcha',
                //'pc/site/config' => 'pc/site/config',
            ],
        ]
    ],
    'modules' => [
        //api模块
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
        //后台api模块
        'backend' => [
            'class' => 'api\modules\backend\Module',
        ]
    ],
    'params' => $params,
];
