<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020-6-10 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\controllers;

use Yii;
use yii\web\UploadedFile;
use common\models\UploadForm;

/**
 * Api 统一上传控制器
 * Uploader controller for the `api`
 */
class UploaderController extends AuthController
{
    /**
     * 图片上传配置
     */
    public function actionConfig()
    {
        $config = [
            'uploadImageMaxSize' => Yii::$app->config->getSiteItem('uploadImageMaxSize'),
            'uploadImageExt' => Yii::$app->config->getSiteItem('uploadImageExt'),
            'uploadVideoMaxSize' => Yii::$app->config->getSiteItem('uploadVideoMaxSize'),
            'uploadVideoExt' => Yii::$app->config->getSiteItem('uploadVideoExt'),
            'uploadFileExt' => Yii::$app->config->getSiteItem('uploadFileExt'),
            'uploadFileMaxSize' => Yii::$app->config->getSiteItem('uploadFileMaxSize'),
            'uploadAudioExt' => Yii::$app->config->getSiteItem('uploadAudioExt'),
            'uploadAudioMaxSize' => Yii::$app->config->getSiteItem('uploadAudioMaxSize'),
        ];
        return Yii::$app->api->success($config);
    }

    /**
     * 表单形式上传图片
     * @throws \yii\base\InvalidConfigException
     */
    public function actionImage()
    {
        $this->validateIsPost();
        $model = new UploadForm(['scenario' => 'image']);
        $model->imageFile = UploadedFile::getInstanceByName('imageFile');
        if ($res = $model->uploadImage(Yii::$app->user->getId())) {
            return Yii::$app->api->success($res);
        }

        $message = $model->getFirstError('imageFile');
        return Yii::$app->api->error($message ? $message : '上传失败');
    }

    /**
     * 表单形式上传视频
     * @throws \yii\base\InvalidConfigException
     */
    public function actionVideo()
    {
        $this->validateIsPost();
        $model = new UploadForm(['scenario' => 'video']);
        $model->videoFile = UploadedFile::getInstanceByName('videoFile');
        if ($res = $model->uploadVideo(Yii::$app->user->getId())) {
            return Yii::$app->api->success($res);
        }

        $message = $model->getFirstError('videoFile');
        return Yii::$app->api->error($message ? $message : '上传失败');
    }

    /**
     * 表单形式上传音频
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAudio()
    {
        $this->validateIsPost();
        $model = new UploadForm(['scenario' => 'audio']);
        $model->audioFile = UploadedFile::getInstanceByName('audioFile');
        if ($res = $model->uploadAudio(Yii::$app->user->getId())) {
            return Yii::$app->api->success($res);
        }

        $message = $model->getFirstError('audioFile');
        return Yii::$app->api->error($message ? $message : '上传失败');
    }

    /**
     * 表单形式上传文件
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFile()
    {
        $this->validateIsPost();
        $model = new UploadForm(['scenario' => 'file']);
        $model->file = UploadedFile::getInstanceByName('file');
        if ($res = $model->uploadFile(Yii::$app->user->getId())) {
            return Yii::$app->api->success($res);
        }

        $message = $model->getFirstError('file');
        return Yii::$app->api->error($message ? $message : '上传失败');
    }

    /**
     * 表单形式上传图片 不加水印
     * @throws \yii\base\InvalidConfigException
     */
    public function actionImageNoWater()
    {
        $this->validateIsPost();
        $model = new UploadForm(['scenario' => 'image']);
        $model->imageFile = UploadedFile::getInstanceByName('imageFile');
        if ($res = $model->uploadImage(Yii::$app->user->getId())) {
            return Yii::$app->api->success($res);
        }

        $message = $model->getFirstError('imageFile');
        return Yii::$app->api->error($message ? $message : '上传失败');
    }
}
