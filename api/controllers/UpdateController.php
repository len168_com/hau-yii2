<?php /** @noinspection ALL */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/5/25 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\controllers;

use Yii;

/**
 * 无需登录 更新 接口
 * @package api\controllers
 */
class UpdateController extends BaseController
{
    /**
     * app更新
     */
    public function actionApp()
    {
        $this->validateIsPost();
        $appid = Yii::$app->getRequest()->post('appid');
        $version = Yii::$app->getRequest()->post('version');
        $resVersion = Yii::$app->getRequest()->post('resVersion');
        $os = Yii::$app->getRequest()->post('os');
        if ($version < Yii::$app->config->getSiteItem('appVersionNum')) {
            // app整包更新
            return Yii::$app->api->success([
                'version' => Yii::$app->config->getSiteItem('appVersionNum'),
                'upType' => 'app',
                'note' => Yii::$app->config->getSiteItem('appAndroidUpInfo'),
                'upLink' => $os == 'Android' ? Yii::$app->config->getSiteItem('appAndroidUpLink') : Yii::$app->config->getSiteItem('appiOSUpLink')
            ]);
        } elseif ($resVersion < Yii::$app->config->getSiteItem('appResVersionNum')) {
            //资源包更新
            return Yii::$app->api->success([
                'version' => Yii::$app->config->getSiteItem('appResVersionNum'),
                'upType' => 'wgt',
                'note' => '',
                'upLink' => $os == 'Android' ? Yii::$app->config->getSiteItem('appAndroidResUpLink') : Yii::$app->config->getSiteItem('appiOSResUpLink')
            ]);
        }
        return Yii::$app->api->error('已是最新版本');
    }
}
