<?php
/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 19-1-12 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\controllers;

use Yii;
/**
 * 支付 接口
 * Class AccountController
 * @package api\modules\v1\controllers
 * @author toshcn <toshcn@foxmail.com>
 */
class PaymentController extends BaseController
{
    /**
     * 支付宝接口回调
     */
    public function actionAlipayNotify()
    {
        $this->validateIsPost();
        $data = Yii::$app->getRequest()->post();
        if (isset($data['out_trade_no']) && !empty($data['out_trade_no'])) {
            //todo
            // 回调成功，打印通知
            echo 'success ';
            exit();
        }
    }
}
