<?php /** @noinspection ALL */

/**
 * @link https://www.len168.com
 * @copyright Copyright (c) 2020/5/25 len168.com
 * @author toshcn <toshcn@foxmail.com>
 */

namespace api\controllers;

use Yii;
use common\models\SmsCaptchaForm;

/**
 * 无需登录 短信 接口
 * @package api\controllers
 */
class SmsController extends BaseController
{
    /**
     * 无需登录权限 获取短信验证码
     */
    public function actionSmsCaptcha()
    {
        $this->validateIsPost();
        $captcha = Yii::$app->getRequest()->post('captcha');
        // 验证图形验证码
        if (!Yii::$app->controller->createAction('captcha')->validate($captcha, (bool) Yii::$app->config->getSiteItem('captchaCaseSensitive'))) {
            return Yii::$app->api->error('图形验证码不正确');
        }
        $model = new SmsCaptchaForm();
        $model->countryCode = Yii::$app->getRequest()->post('country');
        $model->mobile = Yii::$app->getRequest()->post('mobile');
        $model->label = Yii::$app->getRequest()->post('label');
        if ($model->validate() && $model->sendCaptcha()) {
            return Yii::$app->api->success();
        }
        if ($model->hasErrors()) {
            return Yii::$app->api->error($model->getFirstErrorMessage());
        }

        return Yii::$app->api->error('短信验证码发送失败');
    }
}
