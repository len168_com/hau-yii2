<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Hau is a Yii2 API Admin Template</h1>
    <br>
</p>

Hau 是用Yii2框架搭建的API模板 [Yii 2](http://www.yiiframework.com/) .

后台使用 vue + ElementUI
后台前端项目：[hau-vue-admin-template](https://gitee.com/len168_com/hau-vue-admin-template)

Documentation is at [README.md](https://gitee.com/len168_com/hau-yii2/blob/master/README.md).

[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-advanced.svg)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-advanced.svg)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-advanced.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-advanced)

DIRECTORY STRUCTURE
-------------------

```
api
    components/          api组件类目录
    config/              api配置
    controllers/         控制器类存放目录
    modules/             模块分类目录
    runtime/             运行时目录
    tests/               测试相关：如单元测试
    web/                 Web 应用根目录，包含 Web 入口文件
       assets/           包含 Yii 发布的资源文件（javascript 和 css）
       index.php         应用入口文件
common
    actions/             公共控制器动作类目录
    components/          公共组件类目录
    config/              公共配置
    fixtures/            测试夹具目录
    language/            国际化语言相关
    mail/                邮件视图view存放目录
    models/              公共模型类和表单类目录 模型类与数据库表一一对应
    rbac/                角色与权限相关的规则类存放目录
    tests/               测试相关：如单元测试
    widgets/             小部件类存放目录 本项目未用到
common
    config/              公共配置
    mail/                邮件视图view存放目录
    models/              公共模型类和表单类目录 模型类与数据库表一一对应
    tests/               测试相关：如单元测试
console
    config/              控制台配置
    controllers/         控制台控制器类存放目录 (commands)
    migrations/          数据库迁移文件目录
    models/              控制台模型类目录
    runtime/             运行时目录
vendor/                  composer第三方依赖包目录
environments/            与环境相关的配置文件目录
upload/                  附件上传目录
vagrant/                 虚拟机相关
.gitignore               Git仓库忽略文件配置清单
composer.json            Composer第三方包依赖配置文件
docker-composer.json     Docker容器配置
init                     Yii初始化脚本（linux）
init.bat                 Yii初始化脚本（windows）
LICENSE.md               开源协议
README.md                本说明文件
requirements.php         Yii运行环境检测脚本
Vagrantfile              虚拟机配置文件
yii                      Yii控制台命令入口（linux）：如shell控制台下运行php yii 可以看到yii命令的帮助说明
yii.bat                  Yii控制台命令入口（windows）：如cmd控制台下运行./yii.bat 可以看到yii命令的帮助说明
yii_test                 Yii测试控制台命令入口（linux）：如shell控制台下运行php yii_test 可以看到yii命令的帮助说明
yii.bat                  Yii测试控制台命令入口（windows）：如cmd控制台下运行./yii_test.bat 可以看到yii命令的帮助说明
```

Installation

### 克隆仓库代码到本地
```
git clone https://gitee.com/len168_com/hau-yii2.git
```

### 安装 Composer
如果还没有安装 Composer，你可以按 [getcomposer.org](https://getcomposer.org/download/) 中的方法安装。 在 Linux 和 Mac OS X 中可以运行如下命令：

```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
```

在 Windows 中，你需要下载并运行 [Composer-Setup.exe](https://getcomposer.org/Composer-Setup.exe)。

如果遇到任何问题或者想更深入地学习 Composer， 请参考 [Composer 文档](https://getcomposer.org/doc/)。 如果你已经安装有 Composer 请确保使用的是最新版本， 你可以用 composer self-update 命令更新 Composer 为最新版本。

在本指南中，所有 composer 命令都假定您已经安装了全局 的 composer， 这样它可以作为 composer 命令。如果您在本地目录中使用 composer.phar， 则必须相应地调整示例命令。

如果您之前已安装 Composer，请确保使用最新版本。 您可以通过运行 composer self-update 来更新Composer。

> 注意： 在安装 Yii 期间，Composer 需要从 Github API 请求很多信息。 请求的数量取决于您的应用程序所依赖的数量， 并可能大于 Github API 速率限制。如果达到此限制，Composer 可能会要求您提供 Github 登录凭据以获取 Github API 访问令牌。在快速连接上，您可能比 Composer 能够处理的时间早， 因此我们建议您在安装 Yii 之前配置访问令牌。

#### 配置github-oauth

> [创建一个新的token](https://github.com/settings/tokens/new)

配置token 命令中的token请替换为你在Github创建的token
```
composer config [--global] github-oauth.github.com token
```

### 安装composer fxp插件 请确保composer的版本为1.x 不要使用2.0及以上的版本
```
composer global require "fxp/composer-asset-plugin:~1.3"
```

进入代码目录，安装项目的Composer依赖
```
composer install
```

### 初始化项目

根据提示选择运行环境，选项0为dev开发环境，选项1为生产环境。
```
php init
```

配置数据库和缓存

到common\config目录下，main-local.php配置文件，配置好数据库账号密码等。缓存组件建议使用memcache或redis。
```
<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=hau_yii2',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
            'tablePrefix' => 'hau_',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'servers' => [
                [
                    'host' => '127.0.0.1',
                    'port' => 11211,
                    'weight' => 100,
                ],
            ],
            'useMemcached' => false
        ],
    ],
];
```

### 配置web服务器
Api接口 nginx配置如：
```
server {
    listen       80;
    server_name  yourhost.com;
    root   "/home/www/hau_yii2/api/web";
    location / {
        index  index.html index.htm index.php;
        try_files $uri $uri/ /index.php$is_args$args;
        #autoindex  on;
    }
    location ~ \.php(.*)$ {
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_split_path_info  ^((?U).+\.php)(/?.+)$;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  PATH_INFO  $fastcgi_path_info;
        fastcgi_param  PATH_TRANSLATED  $document_root$fastcgi_path_info;
        include        fastcgi_params;
    }
}
```

本地图片 nginx配置如：
```
server {
    listen       80;
    server_name  pic.yourhost.com;
    root   "/home/www/hau_yii2/upload";
    add_header Access-Control-Allow-Origin '*' always;
    add_header 'Access-Control-Expose-Headers' 'Authorization,X-Custom-Header,Content-Disposition';
      
    add_header Accept-Ranges bytes;
    location / { 
        
    }
}
```

### 数据库迁移
在当前代码根目录下，运行控制台命令php yii migrate迁移数据库, 选项yes安装全部迁移。
```
$ php yii migrate
Yii Migration Tool (based on Yii v2.0.41.1)

Creating migration history table "len168_migration"...Done.
Total 6 new migrations to be applied:
        m210416_054206_config
        m210416_054233_carousel
        m210416_054252_attachment
        m210416_054308_sms
        m210416_054316_user
        m210417_053502_menu

Apply the above migrations? (yes|no) [no]:

```

### 安装RBAC数据库迁移
```
php yii migrate --migrationPath=@yii/rbac/migrations

```

### 初始化项目 请按以下顺序运行控制台命令

初始化配置数据
```
php yii init/config

```

初始化权限角色数据
```
php yii rbac/init

```

初始化菜单数据, 并绑定菜单给超级管理员
```
php yii menu/init

php yii menu/set-to-admin

```


创建超级管理员
```
php yii init/create-admin 

```

使用管理员账号登录后台。











